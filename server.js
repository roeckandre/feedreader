var express = require("express");
var session = require("express-session");
var app = express();
var bodyParser = require('body-parser');
var errorHandler = require('errorhandler');
var methodOverride = require('method-override');
var hostname = process.env.HOSTNAME || 'localhost';
var port = 80;
var db = require('./node_modules/mongoskin').db('mongodb://user:password@127.0.0.1:27017/RSSFeed');

var userLib = require("./user.js");
var auth = require('./authenticate.js');

app.use(session({secret: "This is a secret"}));

app.get("/", function (req, res) {
      res.redirect("/index.html");
});

app.get("/registerUser", function (req, res) {
    userLib.add(req, res, db);
});

app.get("/loginUser", function (req, res) {
    userLib.login(req, res, db);
});

app.get("/getAllPosts", function (req, res) {
    var link = decodeURIComponent(req.query.link);
    readURL(link, function(data){
     res.send(data); // send response body
    });
});

app.get("/listSubs", function (req, res) {
    var user = decodeURIComponent(req.query.user);

    auth.restrict(req, res, db, function(ret){  
      if(ret){    
        
        db.collection('subs').find({user:user}).toArray(function(err, result) {
          if(err) {
            res.send("[]");
          } else {
            res.send(JSON.stringify(result));
          }
        });

      }
      else {
        res.send('noauth');
        res.end();
      }
    });

    

});

app.get('/addOrEditSub', function(req, res){
      var info = req.query;

      auth.restrict(req, res, db, function(ret){  
        if(ret){    
        
          db.collection('subs').findOne({link: info.link, user: info.user}, function(err, result) {
            if(result){
              var temp = Object.keys(info);
              var key;
              for(var t = 0; t <  temp.length; t++){
                key = temp[t];
                result[key] = info[key];
              }
              db.collection('subs').save(result, function(err2) {
                if (err2) {
                  res.send("0");            
                } else {        
                  res.send("1");
                }   
              }); 
            }
            else{
              db.collection('subs').insert(info, function(err3, r3) {
                if (err3) {
                  res.send("0");            
                }
                 else {       
                  res.send("1");
                }   
              });
            }
          });
        
        }
        else {
          res.send('noauth');
          res.end();
        }
      });

      
  });

app.get('/removeSub', function(req, res){
      var info = req.query;
      
      auth.restrict(req, res, db, function(ret){  
        if(ret){    
          
          db.collection('subs').findOne({link: info.link, user: info.user}, function(err, result) {
            if (result) {
              db.collection('subs').remove({link: info.link, user: info.user}, function(err, result) {
                if (err) {
                  res.send("0");
                } else {
                  res.send("1");
                }
              });
            }
            else {
              res.send("No Sub with this ID");
            }
          });

        }
        else {
          res.send('noauth');
          res.end();
        }
      });

  });

function readURL(url, cb) {
  var data = "";
  var protocol = url.split("://")[0];
  var request = require(protocol).get(url, function(res) {

    res.on('data', function(chunk) {
      data += chunk;
    });

    res.on('end', function() {
      console.log(JSON.parse(data))
      cb(data);
    })
  });

  request.on('error', function(e) {
    console.log("Got error: " + e.message);
  });
}

app.use(methodOverride());
app.use(bodyParser());
app.use(express.static(__dirname + '/public'));
app.use(errorHandler());

console.log("Simple static server listening at http://" + hostname + ":" + port);
app.listen(port);
